const Koa = require('koa');
const { createServer } = require('https');
const { Server } = require('socket.io');
const fs = require('fs');
const path = require('path');
const server = new Koa();

const port = 8000;

const httpOption = {
  cert: fs.readFileSync(path.join(__dirname, 'certs', 'cert.pem')),
  key: fs.readFileSync(path.join(__dirname, 'certs', 'key.pem')),
};

const httpServer = createServer(httpOption, server.callback());
const io = new Server(httpServer, {
  cors: {
    origin: 'https://localhost:3001',
    method: ['GET', 'POST'],
    credentials: true,
  },
});
io.on('connection', (socket) => {
  console.log('new user connected', socket.id);
});

httpServer.listen(port, () => {
  console.log('Server listen on port ' + port);
});
