#!/bin/zsh

rm certs/*

# set values for certificate DNs
# note: CN is set to different values in the sections below
ORG="000_DEV_ORG"

# set values that the commands will share
SERVER_CRT=server.crt
SERVER_CSR=server.csr
SERVER_KEY=server.key

V3_EXT=v3.ext
KEY_BITS=2048
VALID_DAYS=360


##################################
# SERVER CERTIFICATE
##################################
echo
echo "Creating server certificate..."
CN="000_DEV_SERVER"
openssl genpkey -algorithm RSA -pkeyopt rsa_keygen_bits:$KEY_BITS -out $SERVER_KEY
openssl req -new -key $SERVER_KEY -subj "/CN=$CN/O=$ORG" -out $SERVER_CSR
openssl x509 -days $VALID_DAYS -req -in $SERVER_CSR -CAcreateserial -CA $CA_CRT -CAkey $CA_KEY -extfile $V3_EXT -out $SERVER_CRT
echo "Done."



